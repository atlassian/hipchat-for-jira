package com.atlassian.labs.hipchat.test.mock;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.hipchat.plugins.api.client.HipChatClient;
import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.user.MockUser;
import com.atlassian.labs.jira.notification.HipChatMessageRenderer;
import com.atlassian.labs.jira.workflow.HipChatPostFunction;
import com.atlassian.labs.jira.workflow.HipChatPostFunctionExecutorService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;

import java.util.Map;

/**
 * Overrides some methods for test purpose to avoid calls to not really static helpers (that use {@link com.atlassian.jira.component.ComponentAccessor})
 */
public class HipChatPostFunctionTestOverrider extends HipChatPostFunction {
    private final boolean issuePresent;
    private final boolean jqlMatches;
    private final IssueIndexManager issueIndexManager;

    public HipChatPostFunctionTestOverrider(ApplicationProperties applicationProperties, SearchService searchService, HipChatClient hipChatClient,
                                            HipChatConfigurationManager configurationManager,
                                            HipChatPostFunctionExecutorService executorService,
                                            HipChatMessageRenderer messageRenderer,
                                            boolean issuePresent, boolean jqlMatches, IssueIndexManager issueIndexManager) {
        super(applicationProperties, searchService, hipChatClient, configurationManager, executorService, messageRenderer);
        this.issuePresent = issuePresent;
        this.jqlMatches = jqlMatches;
        this.issueIndexManager = issueIndexManager;
    }

    @Override
    protected User getCaller(Map transientVars, Map args) {
        return new MockUser((String) args.get("username"));
    }

    @Override
    protected boolean matchesJql(String jql, Issue issue, User caller) throws SearchException {
        return jqlMatches;
    }

    @Override
    protected boolean isIssueStoredInDatabase(Issue issue)
    {
        return issuePresent;
    }

    @Override
    protected IssueIndexManager getIssueIndexManager()
    {
        return issueIndexManager;
    }
}
