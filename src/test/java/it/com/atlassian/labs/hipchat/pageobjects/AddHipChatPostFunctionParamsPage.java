package it.com.atlassian.labs.hipchat.pageobjects;

import com.atlassian.jira.pageobjects.pages.admin.workflow.AddWorkflowTransitionFunctionParamsPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

public class AddHipChatPostFunctionParamsPage extends AddWorkflowTransitionFunctionParamsPage
{
    @ElementBy(id = "hipchat-jql-text")
    private PageElement jqlInput;

    @ElementBy(id = "hipchatRoomSelect-multi-select")
    private PageElement roomPicker;

    @ElementBy(id = "notifyClients")
    private PageElement notifyClientsInput;

    @ElementBy(id = "message")
    private PageElement messageInput;

    @ElementBy(id = "workflowName")
    private PageElement workflowName;

    @ElementBy(id = "workflowStep")
    private PageElement workflowStep;

    @ElementBy(id = "workflowTransition")
    private PageElement workflowTransition;

    public AddHipChatPostFunctionParamsPage(final String workflowMode, final String workflowName, final String stepNumber, final String transitionNumber, final String pluginModuleKey)
    {
        super(workflowMode, workflowName, stepNumber, transitionNumber, pluginModuleKey);
    }

    @Override
    public TimedCondition isAt()
    {
        return jqlInput.timed().isPresent();
    }

    public AddHipChatPostFunctionParamsPage setJql(final String jql)
    {
        jqlInput.clear().type(jql);
        return this;
    }

    public AddHipChatPostFunctionParamsPage setRoom(final String roomName)
    {
        // TODO
        return this;
    }

    public AddHipChatPostFunctionParamsPage setMessage(final String message)
    {
        messageInput.clear().type(message);
        return this;
    }

    public String getWorkflowName()
    {
        return workflowName.getValue();
    }

    public String getWorkflowStep()
    {
        return workflowStep.getValue();
    }

    public String getWorkflowTransition()
    {
        return workflowTransition.getValue();
    }
}
