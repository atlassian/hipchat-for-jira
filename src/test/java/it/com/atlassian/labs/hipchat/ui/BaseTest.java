package it.com.atlassian.labs.hipchat.ui;


import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.admin.workflow.AddWorkflowTransitionFunctionParamsPage;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps.Transition;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps.WorkflowStepItem;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowTransitionPage;
import com.atlassian.jira.pageobjects.pages.admin.workflow.WorkflowHeader;
import com.atlassian.jira.util.UriQueryParser;
import com.atlassian.pageobjects.TestedProductFactory;
import it.com.atlassian.labs.hipchat.pageobjects.AddHipChatPostFunctionParamsPage;
import it.com.atlassian.labs.hipchat.pageobjects.HipChatConfigPage;
import org.junit.Before;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public class BaseTest
{
    protected static final String PROJECT_KEY = "MFP";
    protected static final String WORKFLOW_NAME = "MFP: Simple Issue Tracking Workflow";
    protected static final String STATUS_IN_PROGRESS_ID = "2";
    protected static final String TRANSITION_STOP_PROGRESS_ID = "31";
    protected static final String TRANSITION_CREATE_ID = "1";
    protected static final String TRANSITION_DONE_ID = "71";
    protected static final String PLUGIN_MODULE_KEY = "com.atlassian.labs.hipchat.hipchat-for-jira-plugin:hip-chat-post-function";
    protected static final String TEST_API_BASE_URL = "https://api.hipchat.com";
    protected static final String TEST_API_TOKEN = "6e5c154f413527dfd2da22aea33d63";

    protected JiraTestedProduct jira;

    @Before
    public void setup()
    {
        jira = TestedProductFactory.create(JiraTestedProduct.class);
        jira.backdoor().restoreDataFromResource("TestInstance.xml");
        jira.quickLoginAsAdmin(HipChatConfigPage.class).setApiBaseUrl(TEST_API_BASE_URL).setToken(TEST_API_TOKEN).save();
    }

    protected ViewWorkflowSteps createHipChatPostFunction(final String workflowName, final String stepName, final String transitionName) throws URISyntaxException
    {
        ViewWorkflowSteps steps = jira.goTo(ViewWorkflowSteps.class, workflowName);
        steps.createDraftInMode(WorkflowHeader.WorkflowMode.TEXT);

        final WorkflowStepItem step = getStep(steps, stepName);
        final Transition transition = getTransition(step, transitionName);
        ViewWorkflowTransitionPage transitionPage = steps.goToEditTransition(
                transition.getTransition(),
                steps.getCurrentMode().getName(),
                workflowName,
                step.getStepNumber(),
                transition.getTransitionNumber());

        bindToHipChatPostFunctionPage(transitionPage.goToAddPostFunction().selectAndSubmitByName("Notify HipChat"))
                .setMessage("A custom message")
                .submit();

        // Save draft
        return jira.goTo(ViewWorkflowSteps.class, workflowName, true)
                .openPublishDialog()
                .disableBackup()
                .submitAndGotoViewWorkflow();
    }

    protected ViewWorkflowSteps createHipChatPostFunction(final String workflowName, final String transitionId) throws URISyntaxException
    {
        ViewWorkflowSteps steps = jira.goTo(ViewWorkflowSteps.class, workflowName);
        steps.createDraftInMode(WorkflowHeader.WorkflowMode.TEXT);

        ViewWorkflowTransitionPage transitionPage = jira.goTo(ViewWorkflowTransitionPage.class,
                "draft",
                workflowName,
                "",
                transitionId);

        bindToHipChatPostFunctionPage(transitionPage.goToAddPostFunction().selectAndSubmitByName("Notify HipChat"))
                .setMessage("A custom message")
                .submit();

        // Save draft
        return jira.goTo(ViewWorkflowSteps.class, workflowName, true)
                .openPublishDialog()
                .disableBackup()
                .submitAndGotoViewWorkflow();
    }

    protected WorkflowStepItem getStep(final ViewWorkflowSteps steps, final String stepName)
    {
        for (final WorkflowStepItem step : steps.getWorkflowStepItems())
        {
            if (stepName.equals(step.getStepName()))
            {
                return step;
            }
        }
        return null;
    }

    protected Transition getTransition(final WorkflowStepItem step, final String transitionName)
    {
        for (final Transition transition : step.getTransitions())
        {
            if (transitionName.equals(transition.getTransition().getText()))
            {
                return transition;
            }
        }
        return null;
    }

    private AddHipChatPostFunctionParamsPage bindToHipChatPostFunctionPage(final AddWorkflowTransitionFunctionParamsPage addPostFunction) throws URISyntaxException
    {
        final Map<String, String> urlParams = getUrlParams(addPostFunction.getUrl().replace("&workflowStep&", "&workflowStep=&")); // Ensure URL is valid for parsing
        final String workflowMode = urlParams.get("workflowMode");
        final String workflowName = urlParams.get("workflowName");
        final String stepNumber = urlParams.get("workflowStep");
        final String transitionNumber = urlParams.get("workflowTransition");
        final String pluginModuleKey = urlParams.get("pluginModuleKey");
        return jira.getPageBinder().bind(AddHipChatPostFunctionParamsPage.class, workflowMode, workflowName, stepNumber, transitionNumber, pluginModuleKey);
    }

    private Map<String, String> getUrlParams(String url) throws URISyntaxException
    {
        final UriQueryParser parser = new UriQueryParser();
        return parser.parse(new URI(url));
    }
}
