package com.atlassian.labs.jira.dto.dummy;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.labs.jira.dto.NotificationDto;
import com.atlassian.sal.api.ApplicationProperties;

import java.util.Iterator;

/**
 * A NotificationDao with dummy data. Used for rendering a preview of an issue notification.
 */
public class DummyNotificationDto extends NotificationDto
{
    public DummyNotificationDto(final JiraAuthenticationContext authenticationContext, final ApplicationProperties applicationProperties, final IssueTypeManager issueTypeManager)
    {
        super(applicationProperties.getBaseUrl(), getDummyIssue(getUser(authenticationContext), issueTypeManager), getUser(authenticationContext), "Status A", "Status B", "Transition");
    }

    private static Issue getDummyIssue(final User user, final IssueTypeManager issueTypeManager)
    {
        return new DummyIssue("HC-123", "A test issue", user, getAnIssueType(issueTypeManager));
    }

    private static IssueType getAnIssueType(final IssueTypeManager issueTypeManager)
    {
        final Iterator<IssueType> issueTypes = issueTypeManager.getIssueTypes().iterator();
        return (issueTypes.hasNext()) ? issueTypes.next() : null;
    }

    private static User getUser(final JiraAuthenticationContext authenticationContext)
    {
        return authenticationContext.getUser().getDirectoryUser();
    }
}
