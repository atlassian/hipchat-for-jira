(function ($) {
    AJS.toInit(function () {
        $(".project-config-workflow-edit, .project-config-workflow-remove").click(function(e) {
            e.preventDefault();
            var $this = $(this);
            var workflowName = $this.attr("data-workflow-name");
            var url = $this.attr("data-href");

            $.ajax({
                url: AJS.contextPath() + "/rest/hipchat/1.0/workflow/createDraft",
                type: "POST",
                contentType: "application/json",
                data: workflowName
            }).done(function(data) {
                window.location.assign(url);
            });
        });
    });
})(AJS.$);